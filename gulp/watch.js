'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

gulp.task('watch', ['inject'], function () {
	
	var gutil = require('gulp-util');
	var p = paths.src + '/*.html'
	gutil.log( p );

	  gulp.watch([
		paths.src + '/*.html',
		paths.src + '/app/**/*.scss',
		paths.src + '/app/**/*.js',
		'bower.json'
	  ], ['inject']);
});
