// constants.js

(function() {
    'use strict';

    angular
        .module('reibrsmnt.config.url',[])
        .constant('url_constants', {
            adminFormUrl: "http://192.168.35.198:8080/ReibrsmntSvc/adminform/",
            authDetailsUrl: "http://192.168.35.198:8080/ReibrsmntSvc/userdetail",
            claimApproveUrl: "http://192.168.35.198:8080/ReibrsmntSvc/approve/claims/",
            claimDeleteUrl: "http://192.168.35.198:8080/ReibrsmntSvc/claims/",
           // claimLoadUrl: "http://192.168.35.198:8080/ReibrsmntSvc/claims",
           // claimRequestUrl: "http://192.168.35.198:8080/ReibrsmntSvc/claims/details?",
            //claimSubmitUrl: "http://192.168.35.198:8080/ReibrsmntSvc/claims",
            empSearchUrl: "http://192.168.35.198:8080/ReibrsmntSvc/usersList/?user=",
            loginUrl: "http://192.168.35.198:8080/customerAuth/oauth/token",
            statusReportDownloadUrl: "http://192.168.35.198:8080/ReibrsmntSvc/claims/analysis/report",
            //updateAdminUrl: "http://192.168.35.198:8080/ReibrsmntSvc/updateAdmin/",
            //userClaimUrl: "http://192.168.35.198:8080/ReibrsmntSvc/users/claims/",
            //userConfigDetailUrl:"http://192.168.35.198:8080/ReibrsmntSvc/config/getEmployee?userId=",

            // url for testing purpose..............    
            // claimSubmitUrl: "http://192.168.63.63:8080/ReibrsmntSvc/claims",
        //  claimApproveUrl: "http://192.168.63.63:8080/ReibrsmntSvc/approve/claims/",
        //  loginUrl: "http://192.168.63.63:8080/customerAuth/oauth/token", 
        //  authDetailsUrl: "http://192.168.63.63:8080/ReibrsmntSvc/userdetail",
          claimLoadUrl: "http://192.168.63.40:8080/ReibrsmntSvc/claims",
          claimSubmitUrl: "http://192.168.63.40:8080/ReibrsmntSvc/claims",
         userClaimUrl: "http://192.168.63.40:8080/ReibrsmntSvc/users/claims/",
         claimRequestUrl: "http://192.168.63.40:8080/ReibrsmntSvc/claims/details?",
        //  adminFormUrl: "http://192.168.63.63:8080/ReibrsmntSvc/adminform/",
         updateAdminUrl: "http://192.168.63.40:8080/ReibrsmntSvc/updateAdmin/",
        //  empSearchUrl: "http://192.168.63.63:8080/ReibrsmntSvc/usersList/?user=",
          userConfigDetailUrl: "http://192.168.63.40:8080/ReibrsmntSvc/config/getEmployee?userId=",
        //  loginTestUrl: "http://192.168.63.63:8080/ReibrsmntSvc/userdetail",
        //  claimDeleteUrl: "http://192.168.63.63:8080/ReibrsmntSvc/claims/",
        //  statusReportDownloadUrl: "http://192.168.63.63:8080/ReibrsmntSvc/claims/analysis/report"
        });
})();