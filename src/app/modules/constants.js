// constants.js

(function() {
    'use strict';

    angular
        .module('Reibrsmnt')
        .constant('approve_type', {
            approve: 3,
            reject: 4,
            forward: 5,
            hold: 6,
            cleared: 7
        })
        .constant("adminId", 1 )
        .constant("approverId", 2 )
        .constant("normalUserId", 3 )
        .constant("superAdminId", 4)
        .constant("deliveryDepartmentId", 1 )
        .constant("financeDepartmentId", 2 )
        .constant("ITDepartmentId", 3 )
        .constant("ITSupportDepartmentId", 4 )
        .constant("adminDepartmentId", 4 )
        .constant("talentAcquisitionDepartmentId", 4 )
        .constant("legalDepartmentId", 4 )
        .constant("BDDepartmentId", 4 );
})();