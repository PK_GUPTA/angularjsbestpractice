'use strict';

/**
 * @ngdoc function
 * @name translateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the translateApp
 */
angular.module('Reibrsmnt')
  .controller('MainCtrl', function ($scope,env) {
    // Yeoman part (for tests)
    console.log(env.foo);
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
