(function() {
 'use strict';
 angular
  .module('submission')
  .service('claimService', claimService);

 claimService.$inject = ['$http'];

 function claimService($http) {
  
  var service = {
    submit:submit,
    update:update,
    loadData:loadData,
  };

  function loadData(claimUrl, callback) {
   var req = {
    method: 'GET',
    url: claimUrl,
    headers: {
     'Content-Type': 'application/json'
    },
   };
   $http(req)
    .success(function(response, status) {
     callback(response, status);
    }).error(function(error) {

    });
  };

  function submit(claim, claimUrl, callback) {
    var req = {
      method: 'POST',
      url: claimUrl,
      headers: {
       'Content-Type': 'application/json'
      },
      data: claim
    };
    $http(req)
      .then(function(response) {
        callback(response);
        }, function(response){
          callback(response);
        }
      );
  };

  function update(claim, claimUrl, callback) {
    var req = {
      method: 'POST',
      url: claimUrl + '/' + claim.claimRequestHeaderVo.id,
      headers: {
        'Content-Type': 'application/json'
      },
      data: claim
    };
    $http(req)
      .then(function(response){
          callback(response);
        }, function(response){
          callback(response);
        }
      );
  };

  return service;
 }
})();