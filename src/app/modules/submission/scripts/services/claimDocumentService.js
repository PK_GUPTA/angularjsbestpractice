(function() {
 'use strict';
 angular
  .module('submission')
  .service('documentService', documentService);

 documentService.$inject = [];

 function documentService () {
  var service = {
    rowIndex:rowIndex,
    totalAmount:totalAmount
  };

 function rowIndex(array, id) {
   var index = -1;
   for (var i = 0; i <= array.length; i++) {
    if (array[i].id === id || array[i].detailsId === id) {
     index = i;
     break;
    }
   }
   return index;
  };

 function totalAmount(totalClaimAmount, newAmount, command) {
   if (command == "add") {
    totalClaimAmount = totalClaimAmount + newAmount;
   } else if (command == "delete") {
    totalClaimAmount = totalClaimAmount - newAmount;
   }
   return totalClaimAmount;
  };

  return service;
 }
})();