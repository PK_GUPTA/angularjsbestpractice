(function() {

 'use strict'

 angular.module('Reibrsmnt').
 controller('editCtrl', editCtrl);

 editCtrl.$inject = ['$modalInstance', 'documentService', 'claimService', 'localStorageService','$location','$rootScope', 'claimId', 'claimDetails', 'url_constants'];

 function editCtrl($modalInstance, documentService, claimService,localStorageService,$location, $rootScope, claimId, claimDetails, url_constants) {

  var vm = this;
  vm.showLoader = false; 
  vm.newDocument = {};
  vm.claim = {};
  vm.datepickerId = 0;
  vm.format = 'dd.MM.yyyy';
  vm.claim.claimRequestHeaderVo = {};
  vm.claim.claimRequestHeaderVo.claimedAmount = 0;
  vm.claim.claimRequestHeaderVo = claimDetails.claimRequestHeaderVo;
  vm.claim.claimRequestDetailList = claimDetails.claimRequestDetailList;
  //vm.claim.claimRequestHeaderVo.periodFrom = moment(vm.claim.claimRequestHeaderVo.periodFrom, "DD/MM/YYYY");
  //vm.claim.claimRequestHeaderVo.periodTo = moment(vm.claim.claimRequestHeaderVo.periodTo, "DD/MM/YYYY");
  //vm.claim.claimRequestHeaderVo.periodFrom = moment(vm.claim.claimRequestHeaderVo.periodFrom).format('ll');
  //vm.claim.claimRequestHeaderVo.periodTo = moment(vm.claim.claimRequestHeaderVo.periodTo).format('ll');
  console.log(claimDetails.claimRequestHeaderVo);
  vm.addToClaim = function() {
   vm.index = vm.claim.claimRequestDetailList.length;
   vm.claim.claimRequestDetailList[vm.index] = vm.newDocument;
   vm.claim.claimRequestDetailList[vm.index].fileName = vm.newDocument.file.filename;
   vm.claim.claimRequestDetailList[vm.index].file = vm.newDocument.file.base64;
   //vm.claim.claimRequestDetailList[vm.index].billDate = moment(vm.claim.claimRequestDetailList[vm.index].billDate).format('DD/MM/YYYY');
   vm.index = vm.index + 1;
   vm.claim.claimRequestHeaderVo.claimedAmount = documentService.totalAmount(vm.claim.claimRequestHeaderVo.claimedAmount, vm.newDocument.claimAmount, "add");
   vm.newDocument = {};
  };

  vm.delete = function(detailsId) {
   var claimDetailArray = eval(vm.claim.claimRequestDetailList);
   var rowIndex = documentService.rowIndex(claimDetailArray, detailsId);
   if (rowIndex === -1) {
    alert("Something gone wrong");
   } else {
    vm.claim.claimRequestHeaderVo.claimedAmount = documentService.totalAmount(vm.claim.claimRequestHeaderVo.claimedAmount, vm.claim.claimRequestDetailList[rowIndex].claimAmount, "delete");
    vm.claim.claimRequestDetailList.splice(rowIndex, 1);
    vm.index--;
   }
  };

  vm.submitClaim = function(submitType) {
    vm.showLoader = true;
   var curentUser = localStorageService.get('user');
   vm.claim.claimRequestHeaderVo.claimCurrency = parseInt(vm.claim.claimRequestHeaderVo.claimCurrency, 10);
   vm.claim.claimRequestHeaderVo.claimType = parseInt(vm.claim.claimRequestHeaderVo.claimType, 10);
   //vm.claim.claimRequestHeaderVo.periodFrom = moment(vm.claim.claimRequestHeaderVo.periodFrom).format('DD/MM/YYYY');
   //vm.claim.claimRequestHeaderVo.periodTo = moment(vm.claim.claimRequestHeaderVo.periodTo).format('DD/MM/YYYY');
   vm.claim.claimRequestHeaderVo.managerId = curentUser.managerId;
   vm.claim.claimRequestHeaderVo.employeeId = curentUser.employeeId;
   vm.claim.claimRequestHeaderVo.projectId = vm.claim.claimRequestHeaderVo.projectId;
   vm.claim.claimRequestHeaderVo.submitType = submitType;
   console.log(vm.claim);
   claimService.update(vm.claim, url_constants.claimSubmitUrl, function(response) {
      if(response.status==200){
        // vm.ok();
        window.location.reload();
        $location.path('/dashboard');
      }
      else{
        alert(response.status);
        window.location.reload();
      }
   });

   // vm.claim.claimRequestHeaderVo.managerId= $rootScope.globals.currentUser.managerId;
   // vm.claim.claimRequestHeaderVo.employeeId= $rootScope.globals.currentUser.userId;
   // var claimString = JSON.stringify(vm.claim);
   // console.log(claimString);
   // alert(claimString);
  };

  vm.datepickerPopupOpen = function (inputName){
    switch(inputName) {
      case 'periodFrom':
          vm.periodFromPopup = true;
          break;
      case 'periodTo':
          vm.periodToPopup = true;
          break;
      case 'billDate':
          vm.billDatePopup = true;
          break;
      default:
          alert("Error : Please contact finance department");
    }
  };


  vm.ok = function() {
   $modalInstance.close();
  };

  vm.cancel = function() {
    window.location.reload();
    $modalInstance.dismiss('cancel');
  };

 };

})();