(function() {
 'use strict';
 angular
  .module('submission')
  .controller('ClaimsCtrl', ClaimsCtrl);

 ClaimsCtrl.$inject = ['$rootScope', 'claimService', 'documentService', 'localStorageService','$location','url_constants'];

 function ClaimsCtrl($rootScope, claimService, documentService, localStorageService, $location, url_constants) {
  var vm = this;

  vm.showLoader = false;
  vm.initData = {};
  vm.documentId = 1;
  vm.newDocument = {};
  vm.index = 0;
  vm.claim = {};
  vm.claim.claimRequestHeaderVo = {};
  vm.claim.claimRequestHeaderVo.claimedAmount = 0;
  vm.claim["claimRequestDetailList"] = [];
  vm.periodFromPopup = false;
  vm.periodToPopup = false;
  vm.ui = { alert: false };
  vm.validDaysCount = 0;
  vm.claim.claimRequestHeaderVo.specialApprove = false;


  vm.init = function() {
    vm.showLoader = true;
    claimService.loadData(url_constants.claimLoadUrl, function(response) {
    vm.initData = response;
    console.log(vm.initData);
    vm.showLoader = false;
    });
  };
  
  vm.init();

  vm.datepickerPopupOpen = function (inputName){
    switch(inputName) {
      case 'periodFrom':
          vm.periodFromPopup = true;
          break;
      case 'periodTo':
          vm.periodToPopup = true;
          break;
      case 'billDate':
          vm.billDatePopup = true;
          break;
      default:
          alert("Error : Please contact finance department");
    }
  }

   vm.validDate = function() {
    var claimDate = moment(vm.initData.claimDate).format('DD/MM/YYYY');
    console.log(vm.claim.claimRequestHeaderVo.periodTo);
    var periodTo = moment(vm.claim.claimRequestHeaderVo.periodTo).format('DD/MM/YYYY');
    var _claimDate = moment(claimDate, 'DD/MM/YYYY');
    var _periodTo = moment(periodTo, 'DD/MM/YYYY');
    console.log(_claimDate);
    console.log(_periodTo);
    vm.validDaysCount = _claimDate.diff(_periodTo, 'days');
    console.log(vm.validDaysCount);
     if((vm.validDaysCount)>60){
          vm.ui.alert = true;
          vm.claim.claimRequestHeaderVo.specialApprove = true;
          console.log(vm.claim.claimRequestHeaderVo.specialApprove);
      }
      else{
         vm.ui.alert = false;
         vm.claim.claimRequestHeaderVo.specialApprove = false;
         console.log(vm.claim.claimRequestHeaderVo.specialApprove);
      };
   
  

  vm.removeBill = function() {
   vm.newDocument = {};
   vm.claim.claimRequestDetailList = [];
   vm.index = 0;
   vm.documentId = 1;
   vm.claim.claimRequestHeaderVo.claimedAmount = 0;
  };
  vm.addToClaim = function() {

   vm.newDocument.id = vm.documentId++;
   vm.claim.claimRequestDetailList[vm.index] = vm.newDocument;

   vm.claim.claimRequestDetailList[vm.index].fileName = vm.newDocument.file.filename;
   vm.claim.claimRequestDetailList[vm.index].file = vm.newDocument.file.base64;

   //vm.claim.claimRequestDetailList[vm.index].billDate = moment(vm.claim.claimRequestDetailList[vm.index].billDate).format('DD/MM/YYYY');
   vm.index = vm.index + 1;
   vm.claim.claimRequestHeaderVo.claimedAmount = documentService.totalAmount(vm.claim.claimRequestHeaderVo.claimedAmount, vm.newDocument.claimAmount, "add");
   vm.newDocument = {};
  };
  vm.delete = function(id) {
   var claimDetailArray = eval(vm.claim.claimRequestDetailList);
   var rowIndex = documentService.rowIndex(claimDetailArray, id);
   if (rowIndex === -1) {
    alert("Something gone wrong");
   } else {
    vm.claim.claimRequestHeaderVo.claimedAmount = documentService.totalAmount(vm.claim.claimRequestHeaderVo.claimedAmount, vm.claim.claimRequestDetailList[rowIndex].claimAmount, "delete");
    vm.claim.claimRequestDetailList.splice(rowIndex, 1);
    vm.index--;
   }
  };


  vm.submitClaim = function(submitType) {
    vm.showLoader = true;
    var curentUser = localStorageService.get('user');
    
    /*var claimDate = moment(vm.initData.claimDate).format('DD/MM/YYYY');
    var periodTo = moment(vm.claim.claimRequestHeaderVo.periodTo).format('DD/MM/YYYY');
    var _claimDate = moment(claimDate, 'DD/MM/YYYY');
    var _periodTo = moment(periodTo, 'DD/MM/YYYY');
    console.log(_claimDate);
    console.log(_periodTo);
    var validDaysCount = _claimDate.diff(_periodTo, 'days');
    console.log(validDaysCount);*/
    
    vm.claim.claimRequestHeaderVo.claimCurrency = parseInt(vm.claim.claimRequestHeaderVo.claimCurrency, 10);
    vm.claim.claimRequestHeaderVo.claimType = parseInt(vm.claim.claimRequestHeaderVo.claimType, 10);
    vm.claim.claimRequestHeaderVo.managerId = curentUser.managerId;
    vm.claim.claimRequestHeaderVo.employeeId = curentUser.employeeId;
    //vm.claim.claimRequestHeaderVo.periodFrom = moment(vm.claim.claimRequestHeaderVo.periodFrom).format('DD/MM/YYYY');
    //vm.claim.claimRequestHeaderVo.periodTo = moment(vm.claim.claimRequestHeaderVo.periodTo).format('DD/MM/YYYY');
    vm.claim.claimRequestHeaderVo.projectId = vm.claim.claimRequestHeaderVo.projectId;
    vm.claim.claimRequestHeaderVo.specialApprove = vm.claim.claimRequestHeaderVo.specialApprove;
    console.log(vm.claim.claimRequestHeaderVo.specialApprove);
    vm.claim.claimRequestHeaderVo.submitType = submitType;
          claimService.submit(vm.claim, url_constants.claimSubmitUrl, function(response) {
            if(response.status==200){
              $location.path('/menu');
              vm.showLoader = false;
            }
            else if(response.status==500){
              alert("Sorry, server is not responding.");
              console.log(response.statusText);
              vm.showLoader = false;
            }
         });
      }
 

  };


 }
})();