(function() {
 'use strict';

 angular
  .module('admin')
  .service('dashBoardService', dashBoardService);

 dashBoardService.$inject = ['$http'];

 function dashBoardService($http) {
  //var vm = this;
  var service = {
   getClaims:getClaims,
   getClaim:getClaim,
   deleteClaims:deleteClaims,
  };

  function getClaims(id, claimUrl, callback) {
   var req = {
    method: 'GET',
    url: claimUrl + id,
    headers: {
     'Content-Type': 'application/json'
    },
   };
   $http(req)
    .success(function(response) {
     callback(response);
    });

  };

  function getClaim(claimId, userId, claimUrl, callback) {
   var req = {
    method: 'GET',
    url: claimUrl + 'userId=' + userId + '&claimId=' + claimId,
    headers: {
     'Content-Type': 'application/json'
    },
   };
   $http(req)
    .success(function(response) {
     callback(response);
    });

  };

  function deleteClaims(id, Url, callback) {
    // var claimUrl = Url + id;
    $http({
    method: 'delete',
    url: Url + id
   }).then(function successCallback(response) {
    if (response.status == 200) {
      callback(response);
    };
   }, function errorCallback(response) {
    alert("Something gone wrong !!!");
   });
  };

  return service;
 }
})();