(function() {

   'use strict'
   angular.module('Reibrsmnt')
    .controller('DashboardCtrl', DashboardCtrl);

   DashboardCtrl.$inject = ['$uibModal', 'dashBoardService', '$rootScope', '$location','localStorageService', 'url_constants'];

   function DashboardCtrl($uibModal, dashBoardService, $rootScope, location,localStorageService,url_constants) {

    var vm = this;
    vm.showLoader = true;
    vm.reportFormPopup = false;
    vm.reportToPopup = false;
    vm.currentUser=localStorageService.get("user");
    var currentUser = localStorageService.get("user");
    var role = currentUser.role;
    vm.statusReportDownloadUrl = url_constants.statusReportDownloadUrl; 
    vm.init = function() {
      vm.showLoader = true;
      dashBoardService.getClaims(currentUser.employeeId, url_constants.userClaimUrl,
        function receivedClaims(response) {
          vm.claims = response.claimRequestList;
          vm.claimsToApprove = response.claimApproveList;
          vm.showLoader = false;
      });
    }

    vm.init(); 

    vm.datepickerPopupOpen = function (inputName){
    switch(inputName) {
      case 'reportFrom':
          vm.reportFormPopup = true;
          break;
      case 'reportTo':
          vm.reportToPopup = true;
          break;
      default:
          alert("Error : Please contect finance Team");
    }
  }

    vm.isEmpty = function() {
      if (vm.claims == "")
        return true;
      else
        return false;
    }

    vm.normUser = function() {
      if (role == "NormalUser")
        return true;
      else
        return false;
    }

    vm.editStatus = function(status) {
     if (status == "Draft" || status=="Reject")
      return false;
     else
      return true;
    }



    vm.deleteClaim = function(claimId) {
      var index = -1;
      dashBoardService.deleteClaims(claimId, url_constants.claimDeleteUrl, function(response) {
        if (response.status == 200) {
          var claimArray = eval(vm.claims);
          for (var i = 0; i < claimArray.length; i++) {
            if (claimArray[i].id === claimId) {
              index = i;
              vm.claims.splice(index, 1);
              break;
            };
          };
          if (index == -1) {
          alert("Something gone wrong !!!");
          };
        }
        else {
          alert(response.error);
        };
      });
    };

    vm.claimDetails = function(id) {
     location.path("/menu/claimApprove/" + id);
     vm.showLoader = false;
    };

    vm.reportFromChange = function() {
      vm.minReportTo = vm.reportFrom;
      vm.reportFrom = moment(vm.reportFrom).format('DD/MM/YYYY');
      vm.downloadlink = vm.statusReportDownloadUrl +'?fromDate='+vm.reportFrom+'&toDate='+vm.reportTo;
    }
    vm.reportToChange = function() {
      vm.reportTo = moment(vm.reportTo).format('DD/MM/YYYY');
      vm.downloadlink = vm.statusReportDownloadUrl +'?fromDate='+vm.reportFrom+'&toDate='+vm.reportTo;
    };



    vm.open = function(id, size, status) {
      if (status == 'Draft' || status=='Reject') {
        vm.showLoader = true;
        dashBoardService.getClaim(id,currentUser.employeeId, url_constants.claimRequestUrl, function(response, status) {
          vm.claim = response;
          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/modules/submission/views/edit.html',
            controller: 'editCtrl as vm',
            size: size,
            resolve: {
              claimId: function() {
                return id;
              },
              claimDetails: function() {
                return response;
              }
            }
          });
        });
        // vm.showLoader = false;
      };
    };
   };
})();