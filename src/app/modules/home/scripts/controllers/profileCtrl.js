
(function(){
angular.module('Reibrsmnt')
 .controller('ProfileCtrl', ProfileCtrl);

ProfileCtrl.$inject = ['$scope','localStorageService','$location','$rootScope'];

function ProfileCtrl($scope, localStorageService,$location,$rootScope) {
  /*
     Sidebar Toggle & Cookie Control
  */
  var vm = this;
  vm.showLoader =true;
  var mobileView = 992;
  var currentUser = localStorageService.get('user');

  vm.userName = currentUser.employeeName;
  vm.loc = currentUser.loc;
  vm.manager = currentUser.managerName;
  vm.email = currentUser.mail;
  vm.department = currentUser.department;
  vm.role = currentUser.role;
  //$rootScope.$on('$stateChangeStart', function (event, next, current) {
    vm.path = $location.path();
  //};

 vm.getWidth = function() {
  return window.innerWidth;
 };

 $scope.$watch(vm.getWidth, function(newValue, oldValue) {
  if (newValue >= mobileView) {
   if (angular.isDefined(localStorageService.get('toggle'))) {
    $scope.toggle = !localStorageService.get('toggle') ? false : true;
   } else {
    $scope.toggle = true;
   }
  } else {
   $scope.toggle = false;
  }

 });

 vm.toggleSidebar = function() {
  $scope.toggle = !$scope.toggle;
  localStorageService.set('toggle', $scope.toggle);
 };

 window.onresize = function() {
  $scope.$apply();
 };
  vm.showLoader =false;
}
})();