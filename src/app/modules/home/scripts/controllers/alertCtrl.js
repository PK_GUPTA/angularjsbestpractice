/**
 * Alerts Controller
 */
(function(){
angular
    .module('Reibrsmnt')
    .controller('AlertsCtrl', ['$scope', AlertsCtrl]);

function AlertsCtrl($scope) {
    $scope.alerts = [{
        type: 'success',
        msg: 'Claim No. CLM001 has approved by the manager!'
    }, {
        type: 'danger',
        msg: 'Claim No. CLM003 is still pending'
    }];

    $scope.addAlert = function() {
        $scope.alerts.push({
            msg: 'Another alert!'
        });
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
}
})();