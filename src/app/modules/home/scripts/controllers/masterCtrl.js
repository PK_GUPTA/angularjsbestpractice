/**
 * Master Controller
 */
(function(){
angular.module('Reibrsmnt')
 .controller('MasterCtrl', MasterCtrl);

MasterCtrl.$inject = ['$scope', 'localStorageService'];

function MasterCtrl($scope,localStorageService) {
 /**
  * Sidebar Toggle & Cookie Control
  */
  var vm = this;
  var mobileView = 992;


  $scope.getWidth = function() {
    return window.innerWidth;
  };

  var currentUser = localStorageService.get("user");
  if (currentUser) {
    vm.userName = currentUser.employeeName;
    vm.loc = currentUser.loc;
    vm.manager = currentUser.managerName;
    vm.email = currentUser.mail;
    vm.department = currentUser.department;
    vm.role= currentUser.role;
  }


  $scope.$watch($scope.getWidth, function(newValue, oldValue) {
    if (newValue >= mobileView) {
      if (angular.isDefined(localStorageService.get('toggle'))) {
        $scope.toggle = !localStorageService.get('toggle') ? false : true;
      } else {
        $scope.toggle = true;
      }
      } else {
        $scope.toggle = false;
      }
    });

 vm.toggleSidebar = function() {
  $scope.toggle = !$scope.toggle;
  localStorageService.set('toggle', $scope.toggle);
 };

 window.onresize = function() {
  $scope.$apply();
 };
}
})();