
/* @ngInject */
angular.module('Reibrsmnt', ['home',
							'submission',
							'admin',
							'approval',
							'faq',
							'ui.bootstrap',
	                        'ui.router',
	                        'ngCookies',
	                        'reibrsmnt.config.url',
	                        'ng.httpLoader',
	                        'ui.select']);
