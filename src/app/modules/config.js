'use strict';

/**
 * Route configuration for the Reibrsmnt module.
 */
angular
 .module('Reibrsmnt')
   .config(['$stateProvider', '$urlRouterProvider','$httpProvider','httpMethodInterceptorProvider',
    function($stateProvider, $urlRouterProvider,$httpProvider,httpMethodInterceptorProvider) {

     // For unmatched routes
      httpMethodInterceptorProvider.whitelistDomain('http://192.168.35.198:8080');
      $urlRouterProvider.otherwise('/login');

      $urlRouterProvider.when('/menu', '/menu/dashboard');
      $urlRouterProvider.when('/profile', '/menu/profile');
      $urlRouterProvider.when('/dashboard', '/menu/dashboard');
      $urlRouterProvider.when('/tables', '/menu/tables');
      $urlRouterProvider.when('/claim', '/menu/claim');
      $urlRouterProvider.when('/claimApprove', '/menu/claimApprove');
      $urlRouterProvider.when('/admin', '/menu/admin');
      $urlRouterProvider.when('/faq', '/menu/faq');
     // $urlRouterProvider.otherwise('/login');

     // Application routes
     $stateProvider
      .state('base', {
       abstract: true,
       url: '',
       templateUrl: 'app/modules/base.html'
      })
      .state('login', {
       url: '/login',
       parent: 'base',
       templateUrl: 'app/modules/admin/views/login.html',
       controller: 'LoginCtrl as vm'
      })
      .state('menu', {
       url: '/menu',
       parent: 'base',
       templateUrl: 'app/modules/home/views/main-menu.html'
      })
      .state('profile', {
       url: '/profile',
       parent: 'menu',
       templateUrl: 'app/modules/home/views/profile.html',
       controller: 'ProfileCtrl as vm'
      })
      .state('dashboard', {
       url: '/dashboard',
       parent: 'menu',
       templateUrl: 'app/modules/home/views/dashboard.html',
       controller: 'DashboardCtrl as vm'
      })
      .state('claimApprove', {
       url: '/claimApprove/:claimId',
       parent: 'menu',
       templateUrl: 'app/modules/approval/views/claimApprove.html',
       controller: 'ClaimApproveCtrl as vm',
       params: {
        claimId: {
         value: 0
        }
       }
      })
      .state('admin', {
       url: '/admin',
       parent: 'menu',
       templateUrl: 'app/modules/admin/views/admin.html',
       controller: 'adminConfigCtrl as vm'
      })
      .state('claim', {
       url: '/claim',
       parent: 'menu',
       templateUrl: 'app/modules/submission/views/claim.html',
       controller: 'ClaimsCtrl as vm'
      })
      .state('faq', {
       url: '/faq',
       parent: 'menu',
       templateUrl: 'app/modules/faq/views/faqDetails.html',
       controller: 'faqCtrl as vm'
      });
     
      $httpProvider.interceptors.push('rmbHttpInterceptor');
    }
    
   ])

  .factory('rmbHttpInterceptor', ['$log','$injector', function($log,$injector) { 
    
   var rmbHttpInterceptor = {
      request:request,
      requestError:requestError,
      response:response,
      responseError:responseError
    }

    function request (config) {

      var localStorageService = $injector.get('localStorageService');
      var token = localStorageService.get('token')
   
      config.headers = config.headers || {}; 
      config.headers.Authorization = 'Bearer ' + token;    

      //$log.debug('Request : config.headers.Authorization');          
      return config;
    }

    function requestError (rejection) {
      //$log.debug('Requested : ERROR *******************');
      return rejection;
    }

    function response (response) {
      //$log.debug('Response : <<<<<<');
      //console.log(response);
      return response;
    }

    function responseError (rejection) {
      //$log.debug('Response  : ERROR *******************');
      return rejection;
    }

    return rmbHttpInterceptor;
  }])

 .run(['$rootScope', '$location', '$http','localStorageService',
    function($rootScope, $location, $http, localStorageService) {
        //keep user logged in after page refresh
                $rootScope.token = localStorageService.get('token') || {};
                // if ($rootScope.user) {
                //     //$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
                // }
                $rootScope.$on('$stateChangeStart', function (event, next, current) {
                  $rootScope.token = localStorageService.get('token') || {};
                  //console.log("locationChangeStart1" + $location.path());
                    // redirect to login page if not logged in
                    if ($location.path() != '/login' && $rootScope.token.access_token == undefined) {
                        //console.log("locationChangeStart");
                        //$location.path('/login');       // after testing uncomment it.
                    }
                });
  
  }]);