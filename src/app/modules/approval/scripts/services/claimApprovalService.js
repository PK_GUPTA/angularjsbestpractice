(function() {
 'use strict';
 angular
  .module('submission')
  .factory('approvalService', approvalService);

 approvalService.$inject = ['$http'];

 function approvalService($http) {

  var service = {
   loadClaim: loadClaim,
   submitclaim: submitclaim
  };

  function loadClaim(id, userId, url, callback) {
   var req = {
    method: 'GET',
    url: url + 'userId=' + userId + '&claimId=' + id,
    headers: {
     'Content-Type': 'application/json'
    },
   };

   $http(req)
    .then(function(response) {
     callback(response);
    });
  };

  function submitclaim(claimData, url, id, callback) {
   var req = {
    method: 'POST',
    url: url + id,
    headers: {
     'Content-Type': 'application/json'
    },
    data: claimData
   };
   $http(req)
    .then(function(response) {
     callback(response);
    },function(response) {
     callback(response);
    });
  };
  return service;
 };

})();