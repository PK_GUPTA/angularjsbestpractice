  (function() {
 'use strict';

 angular
  .module('approval')
  .controller('ClaimApproveCtrl', ClaimApproveCtrl);

 ClaimApproveCtrl.$inject = ['approvalService', '$stateParams','localStorageService', 'approve_type', '$location', '$rootScope', 'url_constants'];

 function ClaimApproveCtrl(approvalService, stateParams, localStorageService, approve_type,location, $rootScope, url_constants) {
  var vm = this;
  vm.showLoader= true;
  vm.alerts = [ ];
  var curentUser = localStorageService.get('user');
  var claimId = parseInt(stateParams.claimId);

  vm.init = function() {
   approvalService.loadClaim(claimId,  curentUser.employeeId, url_constants.claimRequestUrl, function(response) {
    vm.initData = response.data;
    vm.claimHeader = vm.initData.claimRequestHeaderVo;
    vm.cliamDocuments = vm.initData.claimRequestDetailList;
    vm.approveDetailList = vm.initData.approveDetailsList;
    vm.showLoader= false;
   });
  };

  vm.init();
  vm.closeAlert = function(index) {
        vm.alerts.splice(index, 1);
      };

  vm.submit = function(input) {
    vm.showLoader= true;
    vm.claimdata.employeeId = curentUser.employeeId;
    if (input == "approve") {
      vm.claimdata.claimsStatus = approve_type.approve;
    } 
    else if (input == "reject") {
      vm.claimdata.claimsStatus = approve_type.reject;
    } 
    else if (input == "forward") {
      vm.claimdata.claimsStatus = approve_type.forward;
    }
    else if (input == "hold") {
      vm.claimdata.claimsStatus = approve_type.hold;
    }
    else if (input == "cleared") {
      vm.claimdata.claimsStatus = approve_type.cleared;
    }
    approvalService.submitclaim(vm.claimdata, url_constants.claimApproveUrl, claimId, function(response) {
      if(response.status==200){
        vm.alerts.push({type: 'success', msg: response.statusText});
        location.path("/dashboard");  
        vm.showLoader= false;
      }
      else if(response.status==500){
        // alert(response.statusText);
        vm.alerts.push({type: 'danger', msg: response.statusText});
        vm.showLoader= false;
      }
      else {
        vm.alerts.push({type: 'danger', msg: response.statusText});
        vm.showLoader= false;
      }
    });
  };

 };
})();