(function() {
 	'use strict';
 	angular
 		.module('admin')
 		.controller('adminConfigCtrl', adminConfigCtrl);

 	adminConfigCtrl.$inject = ['$rootScope', 'adminConfigService', '$location','url_constants'];

 	function adminConfigCtrl($rootScope, adminConfigService, $location, url_constants) {
 	 	var vm = this;
 	 	vm.configData={};
 	 	vm.configData.submissionTo = 'manager';
 	 	vm.configData.approveFwdTo ='manager';
 	 	vm.alerts = [ ];
 	 	vm.showLoader = false;

		var init = function () {
			vm.showLoader = true;
		 	adminConfigService.loadData("", url_constants.adminFormUrl, function(response) {
		 		if(response.status==200){
		 			vm.initData = response.data;
		 			vm.showLoader = false;
		 		}
		 		else if(response.status==500){
		 			alert("Server Error,try after some time");
		 			$location.path('/menu');
		 		}
		   });
		};

		vm.getEmpDetail= function() {
			if (vm.employee.userId == null){
				return; 
			}
			else {
				adminConfigService.loadData(vm.employee.userId,url_constants.userConfigDetailUrl,function(response) {
					vm.empDetail = response.data;
					vm.configData.department = vm.empDetail.department;
					vm.configData.roleId = vm.empDetail.roleId;
					vm.configData.approveLimit = vm.empDetail.approveLimit;
					vm.configData.submissionTo = vm.empDetail.submissionTo;
					vm.configData.approveFwdTo = vm.empDetail.approveFwdTo;
					if(vm.empDetail.submissionTo == 'finance') {
					 	vm.submitToEmployees=[];
				 		angular.forEach(vm.empDetail.submitToEmployees, function(object) {
				 			vm.submitToEmployees.push(object.id);	
				 		});
			 		}
					if(vm.empDetail.approveFwdTo == 'finance') {
					 	vm.approveToEmployees=[];
				 		angular.forEach(vm.empDetail.approveToEmployees, function(object) {
				 			vm.approveToEmployees.push(object.id);	
				 		});
			 		}
					console.log(vm.empDetail);
				});
			}
		};

		vm.searchEmployee= function() {
				if(vm.configData.submissionTo == 'finance' && vm.submitToEmployees == null){
					return false;
				}
		};

		vm.searchEmployee= function(searchKey) {
		       adminConfigService.loadData(searchKey,url_constants.empSearchUrl,function(response) {
				vm.empList = response.data;
				});
		};

		vm.closeAlert = function(index) {
    		vm.alerts.splice(index, 1);
  		};

	 	vm.updateAdmin = function() {
	 		vm.showLoader = true;
	 		vm.configData.userId=parseInt(vm.employee.userId);
	 		vm.configData.department = parseInt(vm.configData.department, 10);
	 		vm.configData.roleId = parseInt(vm.configData.roleId, 10);
	 		vm.configData.approveLimit = parseInt(vm.configData.approveLimit, 10)
	 		console.log(vm.configData.approveLimit);
	 		vm.configData.submitToEmployees = [];
	 		if(vm.configData.submissionTo == 'finance') {
	 			if(vm.submitToEmployees==null || vm.submitToEmployees==[]){
	 				alert("Please select submit to Finance Person");
	 				return vm.configData;
	 			}
		 		for(var i=0; i < vm.submitToEmployees.length ; i++) {
		 			angular.forEach(vm.initData.financePersonList, function(object) {
		 				if(object.id == vm.submitToEmployees[i]){
		 					vm.configData.submitToEmployees.push(object);	
		 				}
					});
		 		}
	 		};

	 		vm.configData.approveToEmployees = [];
	 		if(vm.configData.approveFwdTo == 'finance') {
	 			if(vm.approveToEmployees==null || vm.approveToEmployees==[]){
	 				alert("Please select Forward to Finance Person");
	 				return;
	 			}
		 		for(var i=0; i < vm.approveToEmployees.length ; i++) {
		 			angular.forEach(vm.initData.financePersonList, function(object) {
		 				if(object.id == vm.approveToEmployees[i]){
		 					vm.configData.approveToEmployees.push(object);	
		 				}
					});
		 		}
	 		};

	  		adminConfigService.update(vm.configData, url_constants.updateAdminUrl, function(response) {
	  			if(response.status==200) {
	  				vm.showLoader = false;
	  				// alert("Data saved succesfylly");
	  				vm.alerts.push({type: 'success', msg: vm.employee.userName + ' : Successfully updated '});
	  			}
	  			else if(response.status==500){
	  				vm.showLoader = false;
	  				//alert(response.statusText);
	  				vm.alerts.push({type: 'danger', msg: vm.employee.userName  + ' : ' + response.statusText});
	  				vm.configData ='';
	  			}
	  			else {
	  				vm.alerts.push({type: 'danger', msg: vm.employee.userName  + ' : ' + response.statusText});
	  			}	   			
			});
	  	};

		init();
 	}
})();