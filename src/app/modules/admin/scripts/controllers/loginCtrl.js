(function() {
  'use strict';
  angular
    .module('admin')
    .controller('LoginCtrl', LoginCtrl);
  LoginCtrl.$inject = ['$location', 'authenticationService', 'userDetailsService','url_constants','localStorageService'];

  function LoginCtrl($location, authenticationService,userService, url_constants, localStorageService) {
    var vm = this;
    // reset login status
    userService.removeUserDetails();

    // vm.login = function() {
    //   vm.showLoader = true;
    //   authenticationService.login(vm.employeeCode, vm.password, url_constants.loginUrl, function(response) {
    //     if (response.status == 200) {
    //       var expiredAt = new Date();
    //       expiredAt.setSeconds(expiredAt.getSeconds() + response.expires_in);
    //       response.data.expires_at = expiredAt.getTime();
    //       localStorageService.set('token', response.data);
    //       userService.setCurrentUser(vm.employeeCode, url_constants.authDetailsUrl, function() {
    //         vm = localStorageService.get("user");
    //         $location.path('/menu');
    //         // vm.showLoader = false;
    //       });
    //     } 
    //     else if (response.status == 403) {
    //       vm.error = 'Email or password is incorrect';
    //       vm.showLoader = false;
    //     } 
    //     else {
    //       vm.error = 'Server error; Try after some time';
    //       vm.showLoader = false;
    //     }
    //  });
    // };

    // this login is used for testing only..
    vm.login = function() {
      vm.showLoader = true;
      // authenticationService.login(vm.employeeCode, vm.password, loginTestUrl, function(response) {
      //   if (response.status == 200) {
      //     var expiredAt = new Date();
      //     expiredAt.setSeconds(expiredAt.getSeconds() + response.expires_in);
      //     response.data.expires_at = expiredAt.getTime();
           localStorageService.set('token', 'this is our token');
          userService.setCurrentUser(vm.employeeCode, url_constants.authDetailsUrl, function() {
            vm = localStorageService.get("user");
            $location.path('/menu');
            // vm.showLoader = false;
          });
     //    } 
     //    else if (response.status == 403) {
     //      vm.error = 'Email or password is incorrect';
     //      vm.showLoader = false;
     //    } 
     //    else {
     //      vm.error = 'Server error; Try after some time';
     //      vm.showLoader = false;
     //    }
     // });
    };

  }
})();