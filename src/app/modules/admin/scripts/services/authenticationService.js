  (function() {
   'use strict';

   angular
    .module('admin')
    .service('authenticationService', authenticationService);

   authenticationService.$inject = ['Base64', '$http', '$timeout', 'localStorageService'];

   function authenticationService(Base64, $http, $timeout, localStorageService) {
    //var vm = this;
    var authSvc = {
     login: login,
     logout: logout,
     getToken: getToken,
     hasValidToken: hasValidToken
    };

    // function login(username, password, loginUrl, callback) {
    //   var data = "username=" + username + "&password=" + password +
    //   "&grant_type=password&scope=read%20write&" +
    //   "client_secret=123456&client_id=clientapp";
    //   $http.post(loginUrl, data, {
    //     headers: {
    //       "Content-Type": "application/x-www-form-urlencoded",
    //       "Accept": "application/json",
    //       "Authorization": "Basic " + Base64.encode("clientapp" + ':' + "123456")
    //     }
    //   }).then(function(response) {
    //     callback(response)
    //     return response;
    //   }, function(response) {
    //     callback(response)
    //     return response;
    //   });
    // };

    function login(username, loginUrl, callback) {
      var req = {
        method: 'GET',
        url: loginUrl + username,
        headers: {
         'Content-Type': 'application/json'
        },
       };
       $http(req)
        .then(function(response) {
           //setCurrentUserCallBack(response,localStorageService);
           callback(response);
        });
    };

    function logout() {
     localStorageService.clearAll();

    };

    function getToken() {
     return localStorageService.get('token');
    };

    function hasValidToken() {
     var token = this.getToken();
     return token && token.expires_at && token.expires_at > new Date().getTime();
    };

 

    return authSvc;
   }
  })();