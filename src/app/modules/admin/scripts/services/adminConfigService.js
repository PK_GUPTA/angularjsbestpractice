(function() {
  'use strict';
  angular
    .module('admin')
    .service('adminConfigService', adminConfigService);

  adminConfigService.$inject = ['$http'];

  function adminConfigService ($http) {

    var service = {
      loadData:loadData,
      update:update
    };
    function update(adminVO,adminUpdateUrl,callback) {
      var req = {
        method: 'POST',
        url: adminUpdateUrl,
        headers: {
          'Content-Type': 'application/json'
        },
        data: adminVO
      };
      $http(req)
        .then(function(response) {
         callback(response);
      });
    };

    function loadData(searchKey, Url, callback) {
      var req = {
        method: 'GET',
        url: Url+ searchKey,
        headers: {
          'Content-Type': 'application/json'
        },
      };
      $http(req)
        .then(function (response) {
          callback(response);
          return response;
          }, 
          function (response) {
            callback (response);
            return response;
          }
        )
    };
    return service;
  }
})();