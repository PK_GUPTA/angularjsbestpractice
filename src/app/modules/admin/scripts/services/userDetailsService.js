  (function() {
   'use strict';

   angular
    .module('admin')
    .service('userDetailsService', userDetailsService);

   userDetailsService.$inject = ['Base64', '$http', '$cookieStore', '$rootScope', '$timeout', 'localStorageService'];

   function userDetailsService(Base64, $http, $cookieStore, $rootScope, $timeout, localStorageService) {
    //var vm = this;
    var userSvc = {
     getCurrentUser: getCurrentUser,
     setCurrentUser: setCurrentUser,
     removeUserDetails: removeUserDetails
    };

    var currentUser={};

    function setCurrentUser(id, authDetailsUrl,callBack) {

	    var req = {
	      method: 'GET',
	      url: authDetailsUrl + '/' + '?mail=' + id,
	      headers: {
	       'Content-Type': 'application/json'
	      },
	     };
	     $http(req)
	      .success(function(response) {
		       setCurrentUserCallBack(response,localStorageService);
		       callBack("success");
	      });

    }

    function getCurrentUser() {

	   if (!currentUser) {
            currentUser = localStorageService.get('user');
        }
        return currentUser;

    }



    function removeUserDetails() {
      localStorageService.remove('user');
      localStorageService.remove('token');
    };

	function setCurrentUserCallBack(response,localStorageService) {

	     var currentUser= {
	       employeeName: response.employeeName,
	       employeeId: response.employeeId,
	       role: response.role,
	       managerId: response.managerId,
	       mail: response.mail,
	       loc: response.loc,
	       telephoneNumber: response.telephoneNumber,
	       department: response.department,
	       managerName: response.managerCode
	      }

	     localStorageService.set('user', currentUser);

	};

    return userSvc;
   }
  })();